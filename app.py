from flask import Flask, request

import logic
from models import db


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///parking.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    with app.app_context():
        db.drop_all()
        db.create_all()

    @app.teardown_appcontext
    def close_connection(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["GET", "POST"])
    def get_clients():
        if request.method == "POST":
            data = request.get_json()
            return logic.create_client_obj(data, db)

        return logic.get_optional_clients()

    @app.get("/clients/<int:client_id>")
    def get_client(client_id: int):
        return logic.get_optional_clients(client_id)

    @app.post("/parkings")
    def create_parking():
        data = request.get_json()
        return logic.create_parking(data, db)

    @app.post("/client_parkings")
    def check_parking_place():
        data = request.get_json()
        return logic.check_parking_place(data, db)

    @app.delete("/client_parkings")
    def delete_client_parking():
        data = request.get_json()
        return logic.delete_client_parking(data, db)

    return app
