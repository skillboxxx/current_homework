import pytest


def test_create_user(client) -> None:
    client_data = {
        "name": "Boris",
        "surname": "Britva",
        "credit_card": "0000888899991111",
        "car_number": "B111AA799",
    }
    resp = client.post("/clients", json=client_data)

    assert resp.status_code == 201


def test_create_parking(client) -> None:
    parking_data = {
        "address": "address",
        "opened": "True",
        "count_places": 10,
        "count_available_places": 4,
    }
    resp = client.post("/parkings", json=parking_data)

    assert resp.status_code == 201


@pytest.mark.pakring
def test_create_client_parking(client) -> None:
    client_parking_data = {
        "client_id": 2,
        "parking_id": 1,
        # "time_in": datetime.datetime.now(),
        # "time_out": datetime.datetime.now() + datetime.timedelta(hours=1)
    }
    resp = client.post("/client_parkings", json=client_parking_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_remove_client_parking(client) -> None:
    client_parking_data = dict(client_id=1, parking_id=1)

    resp = client.delete("/client_parkings", json=client_parking_data)

    assert resp.status_code == 200


@pytest.mark.parametrize("route", ["/clients"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code in (200, 201)
