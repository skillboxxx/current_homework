import datetime

import pytest

from app import create_app
from models import Client, ClientParking, Parking
from models import db as _db


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://parking.db"

    with _app.app_context():
        _db.create_all()
        client_first_data = {
            "name": "name",
            "surname": "surname",
            "credit_card": "0000888899991111",
            "car_number": "B111AA799",
        }
        client_second_data = {
            "name": "John",
            "surname": "Lebovsky",
            "credit_card": "8888888899991111",
            "car_number": "A111AA799",
        }
        client_first = Client(**client_first_data)
        client_second = Client(**client_second_data)

        parking_data = {
            "address": "address",
            "opened": True,
            "count_places": 10,
            "count_available_places": 4,
        }
        parking = Parking(**parking_data)

        client_parking_data = {
            "client_id": 1,
            "parking_id": 1,
            "time_in": datetime.datetime.now(),
            "time_out": datetime.datetime.now() + datetime.timedelta(hours=1),
        }
        client_parking = ClientParking(**client_parking_data)

        _db.session.add(client_first)
        _db.session.add(client_second)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
