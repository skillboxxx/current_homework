import datetime
import http
from typing import Any, Dict, List, Tuple

from flask_sqlalchemy import SQLAlchemy

from models import Client, ClientParking, Parking


def get_optional_clients(
    client_id: int | None = None,
) -> List[Dict[str, Any]] | Tuple[str, int]:
    if not client_id:
        clients = Client.query.all()
        return [client.to_dict() for client in clients]

    client = Client.query.get(client_id)
    if not client:
        return "Client not found", http.HTTPStatus.NOT_FOUND
    return client.to_dict()


def create_client_obj(
    client_data: Dict[str, Any], db: SQLAlchemy
) -> Tuple[str, int]:
    client = Client(**client_data)
    db.session.add(client)
    db.session.commit()
    return "Client successfully created", http.HTTPStatus.CREATED


def create_parking(
    parking_data: Dict[str, Any], db: SQLAlchemy
) -> Tuple[str, int]:
    parking_data["opened"] = (
        True if parking_data["opened"].lower() == "true" else False
    )
    parking = Parking(**parking_data)
    db.session.add(parking)
    db.session.commit()
    return "Parking successfully created", http.HTTPStatus.CREATED


def check_parking_place(
    data: Dict[str, Any], db: SQLAlchemy
) -> Tuple[str, int]:
    client_id, parking_id = data["client_id"], data["parking_id"]
    client_parking = ClientParking.query.filter_by(
        client_id=client_id
    ).one_or_none()

    if client_parking:
        return "Client already parked", http.HTTPStatus.BAD_REQUEST

    parking = Parking.query.get(parking_id)

    if not parking:
        return "Parking not found", http.HTTPStatus.NOT_FOUND

    if not parking.opened:
        return "Parking is closed", http.HTTPStatus.BAD_REQUEST

    if parking.count_available_places <= 0:
        return "Parking is full", http.HTTPStatus.BAD_REQUEST

    client = Client.query.get(client_id)

    if not client:
        return "Client not found", http.HTTPStatus.NOT_FOUND

    parking.count_available_places -= 1
    time_in = datetime.datetime.now()

    client_parking = ClientParking(
        client_id=client_id, parking_id=parking_id, time_in=time_in
    )
    db.session.add(client_parking)
    db.session.commit()

    return "Successful booking! Drive through", http.HTTPStatus.CREATED


def delete_client_parking(
    data: Dict[str, Any], db: SQLAlchemy
) -> Tuple[str, int]:
    client_id, parking_id = data["client_id"], data["parking_id"]
    client_parking = ClientParking.query.filter_by(
        client_id=client_id, parking_id=parking_id
    ).one_or_none()

    if not client_parking:
        return "Client parking not found", http.HTTPStatus.NOT_FOUND

    if not client_parking.client.credit_card:
        return "Client has no credit card", http.HTTPStatus.BAD_REQUEST

    parking = Parking.query.get(client_parking.parking_id)

    # if not client.credit_card.balance - parking.price >= 0:
    # return 'Client credit card balance is not enough',
    # http.HTTPStatus.BAD_REQUEST
    # client.credit_card.balance -= parking.price

    client_parking.time_out = datetime.datetime.now()
    parking.count_available_places += 1
    # with open('parking.log', 'a', encoding='utf-8') as file:
    #     file.write(client_parking.to_dict() + '\n')
    client_parking.query.delete()
    db.session.commit()

    return "The payment was successful! Goodbye", http.HTTPStatus.OK
